
  
FROM ubuntu:latest AS base
WORKDIR $GOPATH/src/gitlab.com/winterfell_house/gitlab_runner


# ---- Dependencies ----
FROM base AS dependencies
RUN apt update
RUN apt install -y build-essential
RUN apt update
RUN apt install -y wget git gcc
RUN apt update
RUN apt install -y ca-certificates curl gnupg lsb-release
RUN apt update
RUN apt install -y curl
RUN apt update
ENV TZ=America/Sao_Paulo
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN apt-get install -y\
    ca-certificates \
    curl \
    gnupg \
    lsb-release
RUN curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
RUN echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null
RUN apt update
RUN apt install -y docker-ce docker-ce-cli containerd.io
RUN curl -OL https://golang.org/dl/go1.16.7.linux-amd64.tar.gz
RUN tar -C /usr/local -xvf go1.16.7.linux-amd64.tar.gz
ENV GOPATH /go
ENV PATH $GOPATH/bin:/usr/local/go/bin:$PATH
RUN mkdir -p "$GOPATH/src" "$GOPATH/bin" && chmod -R 777 "$GOPATH"


# ---- Build ----
FROM dependencies AS build
COPY . .
ARG VERSION
ARG BUILD
ARG DATE
# RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -v -a -installsuffix cgo -o /go/bin/gitlab_runner  -ldflags "-X main.version=${VERSION} -X main.build=${BUILD} -X main.date=${DATE}" ./

# --- Release ----
FROM gcr.io/distroless/base-debian10 AS image
WORKDIR /
# COPY --from=build /go/bin/gitlab_runner /gitlab_runner
# EXPOSE 8080
USER nonroot:nonroot
ENTRYPOINT ["/gitlab_runner"]